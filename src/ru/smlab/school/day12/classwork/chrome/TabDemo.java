package ru.smlab.school.day12.classwork.chrome;

import ru.smlab.school.day12.classwork.chrome.lists.MyArrayList;
import ru.smlab.school.day12.classwork.chrome.lists.MyLinkedList;

import java.util.Arrays;

public class TabDemo {

    public static void main(String[] args) {
        MyArrayList list = new MyArrayList();
        list.add(new Tab("Youtube", true));
        list.add(new Tab("Spotify", true));
        list.add(new Tab("Spotify", true));
        list.add(new Tab("Spotify", true));
        list.add(new Tab("Google", false));

        list.add(new Tab("Youtube", true));
        list.add(new Tab("Youtube", true));
        list.add(new Tab("Youtube", true));
        list.add(new Tab("Spotify", true));
        list.add(new Tab("Spotify", true));
        list.add(new Tab("Spotify", true));
        list.add(new Tab("Google", false));
    }

    public static void main1(String[] args) {
//        Tab[] browserTabs = new Tab[]{
//                new Tab("Youtube", true),
//                new Tab("Spotify", true),
//                new Tab("Google", false)
//        }; //null null
//
//        browserTabs = Arrays.copyOf(browserTabs, 4);
//        browserTabs[3] = new Tab("",false);

        MyLinkedList list = new MyLinkedList();
        list.add(new Tab("Youtube", true));
        list.add(new Tab("Spotify", true));
        list.add(new Tab("Spotify", true));
        list.add(new Tab("Spotify", true));
        list.add(new Tab("Google", false));
        System.out.println(list.size);
        System.out.println(list);

        System.out.println(list.get(2));

        list.add(3, new Tab("Кинопоиск",false));

        System.out.println("###" + list);

        list.delete(1);
        list.delete(1);

        System.out.println("---" + list);
    }
}
