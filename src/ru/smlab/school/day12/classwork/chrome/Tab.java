package ru.smlab.school.day12.classwork.chrome;

public class Tab {
    String tabName;
    boolean playsSound;

    public Tab(String tabName, boolean playsSound) {
        this.tabName = tabName;
        this.playsSound = playsSound;
    }

    @Override
    public String toString() {
        return "{" + tabName + '}';
    }
}
