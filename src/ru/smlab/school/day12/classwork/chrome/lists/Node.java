package ru.smlab.school.day12.classwork.chrome.lists;

import ru.smlab.school.day12.classwork.chrome.Tab;

public class Node {
    Tab data;
    Node next;
    Node prev;

    public Node(Tab data, Node next, Node prev) {
        this.data = data;
        this.next = next;
        this.prev = prev;
    }
}
