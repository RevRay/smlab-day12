package ru.smlab.school.day12.classwork.chrome.lists;

import ru.smlab.school.day12.classwork.chrome.Tab;

/**
 * Двусвязный список
 */
public class MyLinkedList implements List{

    /**
     * Первый элемент списка
     */
    Node first;

    /**
     * Последний элемент списка
     */
    Node last;

    /**
     * Текущая длина списка
     */
    public int size = 0;

    //CRUD
    //Create Read Update Delete
    //INSERT SELECT UPDATE DELETE

    //Create

    @Override
    public void add(Tab element){
        if (size == 0) { //если в списке пока нету ни одного элемента
            this.first = new Node(element, null, null);
            this.last = this.first;
        } else { //иначе если в списке есть хотя бы один элемент
            Node last = this.last;
            Node newElement = new Node(element, null, last);
            this.last = newElement; //тогда новый элемент становится последним добаленным элементом
            last.next = newElement; //а предпоследний элемент начинает указывать на вновь добавленный (как на следующего соседа)
        }
        size++; //элемент добавлен и размер списка увеличился на +1
    }

    @Override
    public void add(int index, Tab element){
        Node current = getNode(index);
        Node previous = current.prev;
        Node newElement = new Node(element, current, current.prev);

        current.prev = newElement;
        previous.next = newElement;

    }

    //Read

    private Node getNode(int index){
        Node currentElement = first;
        for (int i = 0; i < index; i++) {
            if (currentElement == null) {
                System.out.println(String.format("Элемент по индексу '%d' не существует!", index));
                return null;
            } else { //прыжок
                currentElement = currentElement.next;
            }
        }
        return currentElement;
    }

    @Override
    public Tab get(int index){
        return getNode(index).data;
    }

    //Update
    @Override
    public void set(int index, Tab element){
        getNode(index).data = element;
    }

    //Delete
    @Override
    public void delete(int index){
        Node nodeToBeDeleted = getNode(index);

        Node previous = nodeToBeDeleted.prev;
        Node next = nodeToBeDeleted.next;

        previous.next = next;
        next.prev = previous;

        size--;
    }

    @Override
    public String toString() {
        String result = "";
        Node currentElement = first;
        for (int i = 0; i <= size; i++){
            if (currentElement != null) {
                result += currentElement.data;
                currentElement = currentElement.next;
            } else {
                return result;
            }
            result += ", ";
        }
        return result;
    }
}
