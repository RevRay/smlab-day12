package ru.smlab.school.day12.classwork.chrome.lists;

import ru.smlab.school.day12.classwork.chrome.Tab;

import java.util.Arrays;

/**
 * Реализация класса не завершена!!!
 */
public class MyArrayList implements List {

    int capacity = 10;
    Tab[] elements = new Tab[capacity];

    int size = 0;

    //CRUD
    //Create
    public void add(Tab element){
        if (size + 1 >= capacity) {
            //необхоимо увеличить массив и скопировать старые значения
            capacity = capacity * 2;
            elements = Arrays.copyOf(elements, capacity);
        } else {
            elements[size] = element;
        }
        size++;
    }

    public void add(int index, Tab element){
        elements[index] = element;
        size++;
    }

    //Read

    public Tab get(int index){
        throw new UnsupportedOperationException(); //TODO
    }

    //Update

    public void set(int index, Tab element){
        throw new UnsupportedOperationException(); //TODO
    }

    //Delete

    public void delete(int index){
        throw new UnsupportedOperationException(); //TODO
    }

}
