package ru.smlab.school.day12.classwork.chrome.lists;

import ru.smlab.school.day12.classwork.chrome.Tab;

public interface List {

    /**
     * Добавление элемента в конец списка
     * @param element - добавляемый элемент
     */
    void add(Tab element);

    /**
     * Добавление элемента на определенную позицию (индекс) в списке
     * @param index - позиция
     * @param element - добавляемый элемент
     */
    void add(int index, Tab element);

    /**
     * Получить элемент из списка по индексу
     * @param index
     * @return
     */
    Tab get(int index);

    /**
     * Задать элементу из списка по индексу новое значение
     * @param index
     * @param element
     */
    void set(int index, Tab element);

    /**
     * Удалить элемент по индексу из списка
     * @param index
     */
    void delete(int index);

}
