package ru.smlab.school.day12.classwork.examples;

public class ToStringDemo {
    public static void main(String[] args) {
        Apple[] apples = new Apple[]{
                new Apple("granny smith", 90, 90),
                new Apple("fuji", 100, 120),
                new Apple("red crimson", 90, 120)
        };

        for (Apple a : apples) {
            System.out.println(a);
        }
    }
}
