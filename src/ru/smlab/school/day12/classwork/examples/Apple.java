package ru.smlab.school.day12.classwork.examples;

public class Apple {
    String sort;
    int size;
    int weight;

    public Apple(String sort, int size, int weight) {
        this.sort = sort;
        this.size = size;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Apple{" +
                "sort='" + sort + '\'' +
                ", size=" + size +
                ", weight=" + weight +
                '}';
    }
}
